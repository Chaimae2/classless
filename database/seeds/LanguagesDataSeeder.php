<?php

use Illuminate\Database\Seeder;

class LanguagesDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('languages')->truncate();
        $languages=[
		  'Afrikanns',
		  'Albanian',
		  'Arabic',
		  'Armenian',
		  'Basque',
		  'Bengali',
		  'Bulgarian',
		  'Catalan',
		  'Cambodian',
		  'Chinese (Mandarin)',
		  'Croation',
		  'Czech',
		  'Danish',
		  'Dutch',
		  'English',
		  'Estonian',
		  'Fiji',
		  'Finnish',
		  'French',
		  'Georgian',
		  'German',
		  'Greek',
		  'Gujarati',
		  'Hebrew',
		  'Hindi',
		  'Hungarian',
		  'Icelandic',
		  'Indonesian',
		  'Irish',
		  'Italian',
		  'Japanese',
		  'Javanese',
		  'Korean',
		  'Latin',
		  'Latvian',
		  'Lithuanian',
		  'Macedonian',
		  'Malay',
		  'Malayalam',
		  'Maltese',
		  'Maori',
		  'Marathi',
		  'Mongolian',
		  'Nepali',
		  'Norwegian',
		  'Persian',
		  'Polish',
		  'Portuguese',
		  'Punjabi',
		  'Quechua',
		  'Romanian',
		  'Russian',
		  'Samoan',
		  'Serbian',
		  'Slovak',
		  'Slovenian',
		  'Spanish',
		  'Swahili',
		  'Swedish ',
		  'Tamil',
		  'Tatar',
		  'Telugu',
		  'Thai',
		  'Tibetan',
		  'Tonga',
		  'Turkish',
		  'Ukranian',
		  'Urdu',
		  'Uzbek',
		  'Vietnamese',
		  'Welsh',
		  'Xhosa'];
            foreach ($languages as $key=>$language) {
            	DB::table('languages')->insert(['name'=>$language]);
            }
        
    }
}
