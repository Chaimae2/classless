<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'paypal' => [
        'client_id' => 'AbJ-gzc5C4Jl7jvZFHVnf92OUrZhRTx9xviyadmNLEwnuhYA6DiyONc-zIKvmJ9tjoXzzHbaMbl3kLA8',
        'secret' => 'EA6WBmr_qlpzDFL1pK8jrQoUWmUGLVVOYoIRLHnK2cwc6kZSNTP3TYvWlMNMaGkaU5c15PU3zXza4q0V'
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];
