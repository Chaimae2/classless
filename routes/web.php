<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Auth::routes();
Auth::routes(['verify' => true]);



Route::group(['middleware' => ['auth','verified','premium']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/index', ['as' =>'notifications','uses' =>'NotificationsController@index']);       
        Route::get('/delete', ['as' =>'notifications.deleteall','uses' =>'NotificationsController@deleteAll']);
        Route::get('/{id}/{link?}', ['as' =>'notifications.mark','uses' =>'NotificationsController@markRead']);
        Route::get('/{id}', ['as' =>'notifications.delete','uses' =>'NotificationsController@delete']);       
    });


    Route::group(['prefix' => 'videorequests'], function () {
        Route::get('/', ['as' => 'videorequests', 'uses' => 'VideorequestController@index']);
        Route::get('/create/{id}', ['as' => 'videorequests.create', 'uses' => 'VideorequestController@create']);
        Route::get('/calendar', ['as' => 'videorequests.calendar', 'uses' => 'VideorequestController@calendar']);
        Route::post('/store/{id}', ['as' => 'videorequests.store', 'uses' => 'VideorequestController@store']);
        Route::get('/approve/{id}', ['as' => 'videorequests.approve', 'uses' => 'VideorequestController@approve']);
        Route::get('/delete/{id}', ['as' => 'videorequests.delete', 'uses' => 'VideorequestController@delete']);
        Route::get('/cancel/{id}', ['as' => 'videorequests.cancel', 'uses' => 'VideorequestController@cancel']);
        //Route::post('/update', ['as' => 'profile.update', 'uses' => 'UserController@update']);
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/browse/{id}', ['as' => 'profile.browse', 'uses' => 'UserController@browse']);
        Route::get('/edit', ['as' => 'profile.edit', 'uses' => 'UserController@edit']);
        Route::post('/update', ['as' => 'profile.update', 'uses' => 'UserController@update']);
        // Route::get('/profile/{id}',['as' => 'profile.browse', 'uses' =>'UserController@browse']);
    });


    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        Route::get('create/{id}', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
        Route::post('/store/{id}', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
        Route::post('{id}', ['as' => 'messages.delete', 'uses' => 'MessagesController@delete']);
    });

    Route::group(['prefix' => 'room'], function () {
        // Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        //Route::get('create/{roomName}', ['as' => 'rooms.create', 'uses' => 'RoomController@create']);
        Route::get('/{participant2}', ['as' => 'room', 'uses' => 'RoomController@index']);
        // Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        // Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    });


});

Route::group(['middleware' => ['auth','verified']], function () {

    //Paypal 
    Route::group(['prefix' => 'payments'], function () {
        Route::get('/{transaction_id}/{amount}', ['as'=>'payments.store','uses'=>'PaymentController@store']);
    });

    //Paypal 
    Route::group(['middleware' => ['web']], function () {
        Route::get('payPremium', ['as'=>'payPremium','uses'=>'PaypalController@payPremium']);
        Route::post('getCheckout', ['as'=>'getCheckout','uses'=>'PaypalController@getCheckout']);
        Route::get('getDone', ['as'=>'getDone','uses'=>'PaypalController@getDone']);
        Route::get('getCancel', ['as'=>'getCancel','uses'=>'PaypalController@getCancel']);
    });
});


//Voyager Admin panel
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});