<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials.tags')

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @if(!auth()->guest())
        <script>
            window.Laravel.userId = <?php echo auth()->user()->id; ?>
        </script>
    @endif

</head>
<body>
    <div class="app main">

        <div class="app-body">

              @if (!Auth::guest())
                @desktop
                  @if (!(Route::currentRouteName() === 'payPremium') && !(Route::currentRouteName() === 'verification.notice'))
                      @include('partials.sidebar')
                      @include('partials.navbar')
                      <div class="app-content ">
                          <div class="container-fluid h-100 px-0" style="padding-top: 56px;">
                            @yield('content')
                          </div>
                      </div>
                  @else
                      <div class="app-content">
                          <div class="container-fluid h-100 px-0">
                            @yield('content')
                          </div>
                      </div>
                  @endif
                @elsedesktop
                  @include('mobile')
                @enddesktop

              
              @else
              <div class="app-content" id="body">
                  <div class="container-fluid h-100 px-0">
                    @yield('content')
                  </div>
              </div>
              @endif
        </div>

    </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="{{ asset('js/admin4b.min.js') }}"></script>

@if(!auth()->guest())
    <script >
      @if(Auth::user()->first_login==0)
        <?php App\Http\Controllers\UserController::setFirstLogin(); ?>
        onBoarding();
        
      @endif
      function onBoarding() {
          // introJs().start();
          var intro = introJs();
          intro.setOptions({
            "showBullets": false,
            "showStepNumbers": false,    
          });
          intro.start();
      
      }
      
      window.Echo.private('App.User.' + '{{Auth()->user()->id}}') 
        .notification((notification) => {
          notify(notification.message);
            // console.log(notification.message);
        });

        function notify(msg){
        var html="<div>"+msg+"</div>";
        var html=
          $("#notifications").prepend(html);      
          $("#notifCount").text({{count(Auth::user()->unreadNotifications)}}+1);

        }
    </script>
@endif
</body>
</html>
