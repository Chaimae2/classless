@extends('layouts.app')

@section('content')

<div class="list-group m-4 ">
		<li class="list-group-item border-0 mb-1 d-none d-md-block text-info">
			<div class="row text-center align-items-center " >
				<div class="col-md-2" >
                    <span><strong>Participant</strong></span>
                </div>		
				<span id="startTime" class="col-md-3">
					<span ><strong>Start Time</strong></span>
				</span>
				<span id="endTime" class="col-md-3">
					<span><strong>End Time</strong></span>
				</span>
				<span id="acceptBtn" class="button btn col-md-2">
					<span ><strong>Action/Status</strong></span>
				</span>
				<span id="rejectBtn" class="button btn col-md-2">
					<span ><strong>Action</strong></span>
				</span>
			</div>
		</li>
<!-- 	@foreach(Auth::user()->videorequests_approved() as $videorequest)
		<li class="list-group-item border-0">
			<div class="row text-center align-items-center " >
				<div class="avatar col-md-3 " >
                    <img class="card-img-top m0" src="/storage/avatars/{{ $videorequest->receiver->avatar }}">
                    <span id="from" class="ml-4">{{ $videorequest->receiver->name }}</span>
                </div>		
				<span id="startTime" class="col-md-3">{{ Timezone::convertToLocal($videorequest->getStartDate()) }}</span>
				<span id="endTime" class="col-md-3">{{ Timezone::convertToLocal($videorequest->getEndDate()) }}</span>

				<span id="rejectBtn" class="button btn col-md-3">
				<a href="{{route('videorequests.delete', $videorequest->id)}}" id="from" class="button btn" class="button btn badge badge-danger">
					<span class="badge badge-info" >Cancel</span>
				</a>
			</span>
			</div>
		</li>
	@endforeach -->

		@if(Count(Auth::user()->videorequests_approved())==0)
		<div class="card-body card">
			Sorry, no schedualed video sessions. 
		</div>
		@endif

@foreach(Auth::user()->videorequests_approved() as $videorequest)
			<li class="list-group-item list-group-item-action border-0" data-toggle="collapse" data-target="#details.{{$videorequest->id}}">
				<div class="row text-center align-items-center " >
					<div class="avatar col-sm-2 col-lg-1 px-0" >
	                    
	                	@if(Auth::id()==$videorequest->sender->id)
	                		<img class="card-img-top " style="height: 80px;width:80px;" src="/storage/avatars/{{ $videorequest->receiver->avatar }}">
	                	@else
	                		<img class="card-img-top " style="height: 80px;width:80px;" src="/storage/avatars/{{ $videorequest->sender->avatar }}">
	                	@endif
	                </div>	
	                <div class=" col-sm-2 col-lg-1 px-2" >
	                    <a id="from" class="text-dark" href="{{route('profile.browse',$videorequest->sender->id)}}">
	                    	@if(Auth::id()==$videorequest->sender->id)
	                    		{{ $videorequest->receiver->name }}
	                    	@else
	                    		{{ $videorequest->sender->name }}
	                    	@endif
	                    </a>
	                </div>		
					<span id="startTime" class="col-sm-4 col-lg-3">{{ Timezone::convertToLocal($videorequest->getStartDate()) }}</span>
					<span id="endTime" class="col-sm-4 col-lg-3">{{ Timezone::convertToLocal($videorequest->getEndDate()) }}</span>
					<span id="acceptBtn" class="offset-lg-0 col-6 col-lg-2">
						<span class="badge badge-info" >Approved
							<i class="fas fa-calendar-check p-1"></i>
						</span>
					</span>
					<span id="rejectBtn" class="button btn col-6 col-sm-2 col-lg-2">
						<a href="{{route('videorequests.cancel', $videorequest->id)}}" class="button btn badge badge-danger">Cancel
							<i class="fas fa-times p-1"></i>
						</a>
					</span>
				</div>
				<div class="row align-items-center">
					<div class="col-12 collapse p-2" id="details.{{$videorequest->id}}" >
					
	                	@if($videorequest->details!=null)
	                		<p class="px-5 pt-2">{{$videorequest->details}}</p>
	                	@else
	                		<p >No details.</p>
	                	@endif
		                
		            </div>		
				</div>
			</li>
		@endforeach


</div>

@stop