@extends('layouts.app')

@section('content')

    <nav aria-label="breadcrumb w-100 m-0 p-0">
      <ol class="breadcrumb pt-4 bg-light">
        <li class="breadcrumb-item">Requests</li>
        <li class="breadcrumb-item active" aria-current="page">{{'To: '.$receiver->name}}</li>
      </ol>
    </nav>

        <div class="row w-100">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block col-md-12">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger col-md-12">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

    <div class="card col-sm-12 offset-md-2 col-md-8 d-flex flex-column border border-0">
  
    <div class="card-body col-12">

        <!-- Grid row -->
        <div class="row px-2 align-self-end w-100">
    <form action="{{ route('videorequests.store',$receiver->id)}}" class="col-12" method="post">
        {{ csrf_field() }}
        <div class="col-12">
            <div class="form-group">
                <label class="control-label">Start Time</label>
                <div class='input-group date' data-target-input="nearest" id='startTimePicker'>
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} datetimepicker-input" data-target="#startTimePicker" value="{{ old('startTime') }}" name="startTime"/>
                     @if ($errors->has('startTime'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('startTime') }}</strong>
                                </span>
                            @endif
                    <div class="input-group-append" data-target="#startTimePicker" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label">End Time</label>
                <div class='input-group date' data-target-input="nearest" id='endTimePicker'>
                    <input type="text" class="form-control datetimepicker-input" data-target="#endTimePicker" name="endTime"/>
                    <div class="input-group-append" data-target="#endTimePicker" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>

                    @if ($errors->has('endTime'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('endTime') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="details" class="col-form-label ">{{ __('Details') }}</label>

                <div class="col-md-12 form-group p-0">
                    <textarea id="details" rows="7" class="form-control{{ $errors->has('details') ? ' is-invalid' : '' }}" name="details" autofocus></textarea>

                    @if ($errors->has('details'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('details') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

<!--             <label title="{{ $receiver->name }}"><input type="checkbox" name="recipients[]"
                    value="{{ $receiver->id }}">{!!$receiver->name!!}</label>
     -->
            <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn btn-info btn-block form-control">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</div>

<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script>
    $(function () {
        $('#endTimePicker').datetimepicker();
        $('#startTimePicker').datetimepicker();
    });
</script>
@stop
