@extends('layouts.app')

@section('content')
<div class="list-group m-4">
		<li class="list-group-item border-0 mb-1 d-none d-md-block text-info">
			<div class="row text-center align-items-center " >
				<div class="col-md-2" >
                    <span><strong>Participant</strong></span>
                </div>		
				<span id="startTime" class="col-md-3">
					<span ><strong>Start Time</strong></span>
				</span>
				<span id="endTime" class="col-md-3">
					<span><strong>End Time</strong></span>
				</span>
				<span id="acceptBtn" class="button btn col-md-2">
					<span ><strong>Action/Status</strong></span>
				</span>
				<span id="rejectBtn" class="button btn col-md-2">
					<span ><strong>Action</strong></span>
				</span>
			</div>
		</li>
		@if(Count($user->videorequests())==0)
		<div class="card-body card">
			Sorry, no video sessions requests. 
		</div>
		@endif

	@foreach($user->videorequests() as $videorequest)
		@if($videorequest->to==Auth::id())
			@if($videorequest->status==0)
			<li class="list-group-item list-group-item-action border-0" data-toggle="collapse" data-target="#details.{{$videorequest->id}}">
				<div class="row text-center align-items-center " >
					<div class="avatar col-sm-2 col-lg-1 px-0" >
	                    <img class="card-img-top " style="height: 80px;width:80px;" src="/storage/avatars/{{ $user->avatar }}">
	                </div>	
	                <div class=" col-sm-2 col-lg-1 px-2" >
	                    <a id="from" href="{{route('profile.browse',$videorequest->sender->id)}}" class="text-dark">{{ $videorequest->sender->name }}</a>
	                </div>		
					<span id="startTime" class="col-sm-4 col-lg-3">{{ Timezone::convertToLocal($videorequest->getStartDate()) }}</span>
					<span id="endTime" class="col-sm-4 col-lg-3">{{ Timezone::convertToLocal($videorequest->getEndDate()) }}</span>
					<span id="acceptBtn" class="button btn offset-lg-0 col-6 col-lg-2">
						<a  href="{{route('videorequests.approve', $videorequest->id)}}" class="badge badge-info" >Accept
							<i class="fas fa-check p-1"></i>
						</a>
					</span>
					<span id="rejectBtn" class="button btn col-6 col-sm-2 col-lg-2">
						<a href="{{route('videorequests.cancel', $videorequest->id)}}" class="button btn badge badge-danger">Dismiss
							<i class="fas fa-times p-1"></i>
						</a>
					</span>
				</div>
				<div class="row align-items-center">
					<div class="col-12 collapse p-2" id="details.{{$videorequest->id}}" >
					
	                	@if($videorequest->details!=null)
	                		<p class="px-5 pt-2">{{$videorequest->details}}</p>
	                	@else
	                		<p >No details.</p>
	                	@endif
		                
		            </div>		
				</div>
			</li>
		@endif
	

	@else

		<li class="list-group-item border-0 list-group-item-action" data-toggle="collapse" data-target="#details.{{$videorequest->id}}">
			<div class="row text-center align-items-center ">
				<div class="avatar col-sm-2 col-lg-1 px-0" >
                    <img class="card-img-top " style="height: 80px;width:80px;" src="/storage/avatars/{{ $user->avatar }}">
                </div>	
                <div class="col-sm-2 col-lg-1 px-2" >
                    <a id="from" class="text-dark" href="{{route('profile.browse',$videorequest->sender->id)}}">{{ $videorequest->receiver->name }}</a>
                </div>			
				<span id="startTime" class="col-sm-4 col-lg-3">{{ Timezone::convertToLocal($videorequest->getStartDate()) }}</span>
				<span id="endTime" class="col-sm-4 col-lg-3">{{ Timezone::convertToLocal($videorequest->getEndDate()) }}</span>
				<span id="acceptBtn" class="offset-lg-0 col-6 col-lg-2">
				<span class="badge badge-info" >Waiting
					<i class="fas fa-spinner p-1"></i>
				</span>
				</span>
				<span id="rejectBtn" class="button btn col-6 col-sm-2 col-lg-2">
					<a href="{{route('videorequests.delete', $videorequest->id)}}" id="from" class="button btn" class="button btn badge badge-danger">
						<span class="badge badge-danger p-1" >
						 Cancel
						 	<i class="fas fa-times p-1"></i>
						</span>
					</a>
				</span>
			</div>
			<div class="row align-items-center">
				<div class="col-12 collapse p-2" id="details.{{$videorequest->id}}" >
                    	@if($videorequest->details!=null)
                    		<p class="px-5 pt-2">{{$videorequest->details}}</p>
                    	@else
                    		<p>No details.</p>
                    	@endif
                    
                </div>		
			</div>
		</li>
	@endif
	@endforeach

</div>
@stop