<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Classless</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Arbutus+Slab|Roboto+Slab" rel="stylesheet">
                <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"  integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;

            }

            body{
                background-image: linear-gradient(rgba(218,182,200,0.6), rgba(218,182,200,0.6)), 
                /*url("https://blogs.microsoft.com/uploads/sites/4/2013/08/skype_southkorea_canada4-sitc-blog.png");*/
                url('storage/images/welcome.jpg');
                background-size: 100% 100%;
                background-repeat: no-repeat;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .jumbotron {
                background: none;
                color: #fff;
            }
            .container{
                display: block;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
               
            }

        </style>
    </head>
    <body>
 
    <main role="main" class="container">
        <div class="jumbotron" style="color:#002147;">
            <h1 class="display-3 text-center" style="font-family: 'Arbutus Slab', cursive;">Flatten your classroom's walls!</h1>
            <p class="lead  text-center" style="font-family: 'Roboto Slab', cursive;"></p>
            <hr class="my-4 text-center" style="border-color:#002147;">
            <p class="lead  text-center" style="font-family: 'Roboto Slab', cursive;">Select a partner. Plan a video session. Go!</p>
            <p class="lead text-center">
                <a class="btn btn-info btn-lg" href="{{route('register')}}" role="button">Register Now!</a>
                <a class="btn btn-info btn-lg" href="{{route('login')}}" role="button">Login</a>
            </p>
        </div>
    </main>
    
    </body>
</html>
