@extends('layouts.app')
@section('content')

<div class="container w-100 mx-auto mt-5 ">
	<div class="card row w-100 h-100 justify-content-center d-flex">
	<div class="row card-body h-100 w-100 align-self-center">
		<div class="col-md-6 w-100 h-100">
			<div class="col-12 h-50 avatar d-flex text-center pt-5">
				<img class=" m-auto" 
				style="max-width:300px;max-height:300px;" src="/storage/avatars/{{ $user->avatar }}" />
		    </div>
		   	<div class="col-12 h-50 p-2">
		        <label for="email" class="col-lg-12 font-weight-bold col-form-label">{{ __('About') }}</label>

		        <div class="col-12">
		        	<i class="fas fa-info-circle"></i>
		        	<span> {{$user->about}} </span>
		        </div>
		    </div>
		</div>

		<div class="col-md-6 w-100 my-auto">
			<div class="col-lg-12 p-2">

		        <label for="name" class="col-lg-12 font-weight-bold col-form-label">{{ __('User Name') }}</label>
		        
		        <div class="col-lg-12">
		        	<i class="fa fa-user"></i>
		        	<span> {{$user->name}} </span>
		        </div>
		    </div>
		   	<div class="col-lg-12 p-2">
		        <label for="email" class="col-lg-12 font-weight-bold col-form-label">{{ __('School') }}</label>

		        <div class="col-lg-12">
		        	<i class="fas fa-school"></i>
		        	<span> {{$user->school}} </span>
		        </div>
		    </div>

		   	<div class="col-lg-12 p-2">
		        <label for="email" class="col-lg-12 font-weight-bold col-form-label">{{ __('Country') }}</label>

		        <div class="col-lg-12">
		        	<i class="fas fa-map-marker-alt"></i>
		        	<span> {{$user->country}} </span>
		        </div>
		    </div>

		    <div class="col-lg-12 p-2">
		        <label for="email" class="col-lg-12 font-weight-bold col-form-label">{{ __('Native Language') }}</label>

		        <div class="col-lg-12">
		        	<i class="fas fa-globe-americas"></i>
		        	@if($user->mother_tongue()!=null)
		        	<span> {{$user->mother_tongue()->name}} </span>
		        	@endif
		        </div>
		    </div>
		    <div class="col-lg-12 p-2">
		        <label for="email" class="col-lg-12 font-weight-bold col-form-label">{{ __('Other spoken languages') }}</label>

		        <div class="col-lg-12">
		        	<i class="fas fa-globe-americas"></i>
		        	@if($user->language1()!=null)
		        		<span> {{$user->language1()->name}} </span>
		        	@endif
		        	@if($user->language2()!=null)
		        		<span> {{', '.$user->language2()->name}} </span>
		        	@endif		        </div>
		    </div>



		</div>
		
	</div>

</div>
@stop