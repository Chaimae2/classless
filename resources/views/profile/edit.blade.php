@extends('layouts.app')
@section('content')
<div class="container w-100 mt-2 pb-0">
	<div class="row w-100 justify-content-center">
        <div class="row w-100">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block w-100">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
        </div>

        <div class="card row w-100 justify-content-center m-0 p-0 px-4 border border-0">
            <form method="POST" action="{{ route('profile.update') }}"
            enctype="multipart/form-data" class="row card-body w-100 pb-0">
                <div class="col-lg-6 m-0 p-0 px-4">
                	<div class=" form-group justify-content-center col-lg-12 w-100 m-0 p-0 mt-3 text-center avatar text-center">
        				<img class="rounded-circle" src="/storage/avatars/{{ $user->avatar }}" />

                        @csrf
                        <div class="custom-file form-group col-lg-12  m-0 p-0 mt-4 w-100">
                            <input type="file" class="custom-file-input mb-0" name="avatar" id="avatarFile" >
                             <label class="custom-file-label w-100 
                             text-left" for="customFile text">Choose file...</label>
                       </div>
        			</div>


                    <div class="form-group col-lg-12  m-0 p-0">
                        <label for="name" class="col-lg-12 col-form-label">{{ __('Nickname') }}</label>

                        <div class="col-lg-12  m-0 p-0">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $user->name) }}">

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-lg-12  m-0 p-0">
                        <label for="email" class="col-lg-12 col-form-label">{{ __('E-Mail Address') }}</label>

                        <div class="col-lg-12  m-0 p-0">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email',$user->email) }}" >

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group col-lg-12  m-0 p-0">
                        <label for="password" class="col-lg-12 col-form-label">{{ __('Password') }}</label>

                        <div class="col-lg-12  m-0 p-0">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" value="*******" name="password">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                <div class="form-group col-lg-12  m-0 p-0" >
                    <label for="school" class="col-lg-12 col-form-label ">{{ __('School') }}</label>

                    <div class="col-lg-12  m-0 p-0">
                        <input id="school" type="text" class="form-control{{ $errors->has('school') ? ' is-invalid' : '' }}" name="school" value="{{ old('school',$user->school)}}" >

                        @if ($errors->has('school'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('school') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-lg-12  m-0 p-0">
                    <label for="name" class="col-lg-12 col-form-label">{{ __('Country') }}</label>

                    <div class="col-lg-12  m-0 p-0">
                        <input id="country" type="text" class="form-control{{ $errors->has('Country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country',$user->country) }}" >

                        @if ($errors->has('country'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                </div>
            <div class="col-lg-6 m-0 p-0 px-4">

                <label for="name" class="col-lg-12 col-form-label">{{ __('Native Language') }}</label>
                <select class="custom-select form-group mb-0"  name="mother-tongue">
                    
                    <option value="" selected>
                        {{'Choose...'}}
                    </option>
                    
                    @foreach($languages as $language)
                        @if(Auth::user()->mother_tongue==$language->id)
                        <option value="{{$language->id}}" selected>{{$language->name}}</option>
                        @else
                        <option value="{{$language->id}}">{{$language->name}}</option>
                        @endif
                    @endforeach

                    @if ($errors->has('mother_tongue'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mother_tongue') }}</strong>
                        </span>
                    @endif
                </select>

                <label for="name" class="col-lg-12 col-form-label">{{ __('Other spoken language') }}</label>

                <select class="custom-select form-group col-lg-12 mb-0" name="language1">
                    
                    <option value="" selected>
                        {{'Choose...'}}
                    </option>

                    @foreach($languages as $language)
                        @if(Auth::user()->language1()!=null)
                            @if(Auth::user()->language1()->id==$language->id)
                                <option value="{{$language->id}}" selected>{{$language->name}}</option>
                            @else
                                <option value="{{$language->id}}">{{$language->name}}</option>
                            @endif
                        @else
                            <option value="{{$language->id}}">{{$language->name}}</option>
                        @endif
                    @endforeach

                    @if ($errors->has('language1'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('language1') }}</strong>
                        </span>
                    @endif
                </select>

                <label for="name" class="col-form-label col-lg-12">{{ __('Other spoken language') }}</label>
                <select class="custom-select form-group col-lg-12 mb-0" name="language2">

                    <option value="" selected>
                        {{'Choose...'}}
                    </option>


                    @foreach($languages as $language)
                        @if(Auth::user()->language2()!=null)
                            @if(Auth::user()->language2()->id==$language->id)
                                <option value="{{$language->id}}" selected>{{$language->name}}</option>
                            @else
                                <option value="{{$language->id}}">{{$language->name}}</option>
                            @endif
                        @else
                            <option value="{{$language->id}}">{{$language->name}}</option>
                        @endif
                    @endforeach

                    @if ($errors->has('language2'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('language2') }}</strong>
                        </span>
                    @endif
                </select>

                <div class="form-group col-lg-12  m-0 p-0" >
                    <label for="min-age" class="col-lg-12 col-form-label ">{{ __("Lowest Students' age") }}</label>

                    <div class="col-lg-12  m-0 p-0">
                        <input id="min-age" type="text" class="form-control{{ $errors->has('min-age') ? ' is-invalid' : '' }}" name="min-age" value="{{ old('min-age',$user->min_age)}}" >

                        @if ($errors->has('min-age'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('min-age') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-lg-12  m-0 p-0" >
                    <label for="max-age" class="col-lg-12 col-form-label ">{{ __("Highest Students' age") }}</label>

                    <div class="col-lg-12  m-0 p-0">
                        <input id="max-age" type="text" class="form-control{{ $errors->has('max-age') ? ' is-invalid' : '' }}" name="max-age" value="{{ old('max-age',$user->max_age)}}" >

                        @if ($errors->has('max-age'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('max-age') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group col-lg-12  m-0 p-0">
                    <label for="about" class="col-lg-12 col-form-label ">{{ __('About') }}</label>

                    <div class="col-lg-12 m-0 p-0">
                        <textarea id="about" rows="7" class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}  pb-2" name="about" placeholder="Tell us more about your students and your ptojects.">{{ old('about',$user->about) }}</textarea>

                        @if ($errors->has('about'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('about') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>

                <div class="form-group col-lg-12 d-flex mt-2 mx-0 px-0">
                    <div class="col-sm-12 self-align-center text-center">
                        <button type="submit" class="btn btn-info btn-block">
                            {{ __('Submit') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    //Show file name 
    $('.custom-file-input').on('change', function() { 
       let fileName = $(this).val().split('\\').pop(); 
       $(this).next('.custom-file-label').addClass("selected").html(fileName); 
    });
</script>


@endsection
