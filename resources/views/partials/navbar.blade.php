              <nav class="navbar navbar-expand navbar-light fixed-top" style="background: #002147; color:white;">

                  <div class="dropdown d-lg-none d-md-none">
                    <button class="btn sidebar-btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:transparent;">
                      <i class="fas fa-bars" style="color:white;"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">                  
                      <a class="dropdown-item" href="{{route('home')}}">Home</a>
                      <a class="dropdown-item" href="{{route('videorequests')}}">Video Requests</a>
                      <a class="dropdown-item" href="{{route('videorequests.calendar')}}">Calendar</a>
                      <a class="dropdown-item" href="{{route('profile.edit')}}">Edit profile</a>
                    </div>
                  </div>
                <div class="navbar-brand ml-3" style="color:rgba(218,182,200); font-family: 'Monoton', cursive;font-size: 30px;">
                <a href="{{route('home')}}" style="text-decoration: none;color:inherit;">Classless</a>
                </div>

                <ul class="navbar-nav ml-auto" style="color:white;">
                  <li class="nav-item dropdown" >
                      <a href="#" class="nav-link" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;" data-intro='Here we notify you of the important events.'>
                          <span class="badge badge-pill badge-danger bg-danger notifCount" id="notifCount">{{count(Auth::user()->unreadNotifications)}}</span> 
                          <i class="fas fa-bell" style="color:white;"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" id="notifications">
                      @if(count(Auth::user()->unreadNotifications) == 0)
                      <div>
                        <p class="p-3 text-center">
                          You don't have any unread notification.
                        </p>
                      </div>
                      @else
                      @foreach(Auth::user()->unreadNotifications()->take(4)->get()  as $notification)
                      <a href="{{route('notifications.mark',[$notification->id,$notification->data['link']])}}" class="align-items-center d-flex dropdown-item" id="notif">

                        <div class="col-md-1 align-self-center text-center">  
                          @if($notification->type=='App\Notifications\videoRequestRcd')
                            <i class="fas fa-video-camera "></i>
                          @elseif($notification->type=='App\Notifications\MessageRcd')
                            <i class="fas fa-envelope"></i>
                          @elseif($notification->type=='App\Notifications\videoRequestAccepted')
                            <i class="fas fa-calendar-check-o"></i>
                          @elseif($notification->type=='App\Notifications\VideorequestCanceled')
                            <i class="fas fa-calendar-times "></i>
                          @endif
                        </div>

                        <div class="col-md-12 align-self-center p-2">
                          <span>{{$notification->data['message']}}</span>
                        </div>

                      </a>
                      @endforeach
                      @endif
                      <div class="dropdown-divider"></div>
                      <a href="{{route('notifications')}}" class="dropdown-item" >
                        <div class="text-center">
                        <strong>See all notifications</strong>
                        </div>
                      </a>
                      <div class="dropdown-divider"></div>
                      <a href="{{route('notifications.deleteall')}}" class="dropdown-item" >
                        <div class="text-center">
                        <strong>Mark all as read</strong>
                        </div>
                      </a>
                      </div>
                  </li>

                  <li class="nav-item">
                      <a href="/messages" title="Messages" role="button" class="nav-link" data-intro='Click here to view your inbox.'>
                        <i class="fa fa-envelope" style="color:white;"></i>
                      </a>
                  </li>

                  <li class="nav-item">

                      <a title="boarding" role="button" class="nav-link"
                        onclick="onBoarding()" data-intro='This guides you through the platform.' href="#">
                      <i class="fas fa-question" style="color:white;"></i>
                      </a>
                  </li>

                  <li class="nav-item">

                      <a title="Logout" role="button" class="nav-link"
                        href="{{route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();" data-intro='You can loggout from here.'>
                      <i class="fas fa-power-off" style="color:white;"></i>
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </li>

                </ul>
                
              </nav>