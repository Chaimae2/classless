            <div class="app-sidebar d-none d-md-block" style="padding-top: 56px;">

              <div class="sidebar-header mt-5 avatar" >
                <img src="/storage/avatars/{{ Auth::user()->avatar }}" class=""
                style="height: 150px; width:150px;">
                <p class="username">
                  {{ Auth::user()->name }}<br>
                  <small>{{ Auth::user()->email }}</small>
                </p>
              </div>

              <ul id="sidebar-nav" class="sidebar-nav" data-children=".sidebar-nav-group">
                <li class="sidebar-nav-btn mb-5">
                  <a href="{{route('profile.edit')}}" class="btn btn-block btn-outline-info" data-intro='First thing first, add more details about your students for a better experience.'>
                    Edit Profile
                  </a>
                </li>

                <li class="sidebar-nav-group ">
                  <a href="/home" class="sidebar-nav-link {{Route::currentRouteNamed('home') ? 'active' : ''}}" data-intro='Click here to search a partner.'>
                    Search a collaborator
                  </a>
                </li>
                <li class="sidebar-nav-group">
                  <a href="{{route('videorequests')}}" class="sidebar-nav-link" data-intro='Here you find video requests you sent or received but not yet approved.'>
                    <span class="{{Route::currentRouteNamed('videorequests') ? 'active' : ''}}" >
                    Video Sessions Requests</span>

                    @if(count(Auth::user()->videorequests_waiting()->get())>0)
                    <span class="badge badge-pill bg-danger notifCount" id="notifCount">{{count(Auth::user()->videorequests_waiting()->get())}}</span> 
                    @endif

                  </a>
                </li>

                <li class="sidebar-nav-group">
                  <a href="{{route('videorequests.calendar')}}" class="sidebar-nav-link {{Route::currentRouteNamed('videorequests.calendar') ? 'active' : ''}} " data-intro='Here is your calendar where you can see your schedualed video sessions.'>
                   Schuedualed video sessions
                  </a>

                </li>

              </ul>

            </div>