<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        #cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    /*z-index:9999;*/
    /*display:none;*/
}

@-webkit-keyframes spin {
    from {-webkit-transform:rotate(0deg);}
    to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
    from {transform:rotate(0deg);}
    to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
    </style>
</head>
@extends('partials.tags')
<body style="height:100vh; width:100vw; margin:0px;">
    <div id="cover-spin"></div>
    <!-- <div class="text-center container d-flex w-100 h-100"> -->
    <!-- <span class="fas fa-spinner fa-10x m-auto" style="color:black;"></span> -->
    <!-- </div> -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <script src="https://meet.jit.si/external_api.js"></script>
    <script>
        var api;
        $(document).ready(function() {

            acceptCall("{{$roomName}}");
            $("#cover-spin").hide();
        });

        function videoConferenceLeftListener(object)
        {
            api.dispose();
            window.location = "{{route('home')}}";
        }

            function acceptCall(RoomName) 
            {
                        var domain = "meet.jit.si";
                        var options = {
                            roomName: RoomName,
                            width: '100%',
                            height: '98%',
                            interfaceConfigOverwrite: { 
                                MAIN_TOOLBAR_BUTTONS: [],
                                TOOLBAR_BUTTONS: [            
                                    "microphone", 
                                    "camera",
                                    "desktop",
                                    "sharedvideo",
                                    'hangup',
                                    'fullscreen',
                                    'etherpad'
                                    ],
                                DISABLE_RINGING: false,
                                VIDEO_QUALITY_LABEL_DISABLED: true,
                                SHOW_JITSI_WATERMARK: false,
                                SHOW_WATERMARK_FOR_GUESTS: false,
                                MOBILE_APP_PROMO: false,
                                GENERATE_ROOMNAMES_ON_WELCOME_PAGE: false,
                                DISPLAY_WELCOME_PAGE_CONTENT: false,
                                INVITATION_POWERED_BY: false,
                                FILM_STRIP_MAX_HEIGHT: 0,
                            },
                            disableThirdPartyRequests: true,
                        }
                        api= new JitsiMeetExternalAPI(domain, options);
                        api.addEventListeners({
                            videoConferenceLeft: videoConferenceLeftListener,
                        })
                        // api.executeCommand('displayName', RoomName);
                        // api.executeCommand('toggleShareScreen');
                        // api.executeCommand('avatarUrl', 'https://avatars0.githubusercontent.com/u/3671647');
                        // iframe = api.getIFrame();

            }
    </script>
    </body>
</html>