@extends('layouts.app')

@section('content')

	<div class=" d-flex list-group m-4">
		@if(Count(Auth::user()->unreadNotifications)==0)
		<div class="card-body card">
			Sorry, no new notifications. 
		</div>
		@endif
		@foreach(Auth::user()->unreadNotifications as $notification)
		<li class="list-group-item list-group-item-action border-0">
			<div class="row align-items-center " >
					
					<div class="col-md-2 align-self-center text-center">
				
						@if($notification->type=='App\Notifications\videoRequestRcd')
						<i class="fas fa-video-camera fa-2x "></i>
						@elseif($notification->type=='App\Notifications\MessageRcd')
						<i class="fas fa-envelope fa-2x "></i>
						@elseif($notification->type=='App\Notifications\videoRequestAccepted')
						<i class="fas fa-calendar-check-o fa-2x "></i>
						@elseif($notification->type=='App\Notifications\VideorequestCanceled')
						<i class="fas fa-calendar-times fa-2x "></i>
						@endif
					</div>

					<div class="col-md-5">
						<small>{{Timezone::convertToLocal(Carbon\Carbon::parse($notification->created_at->diffForhumans()))}}</small>
					  	<p class="mt-2">{{$notification->data['message']}}</p>
				  	</div>
				  	
				<span id="viewBtn" class="button btn col-md-2">
					<a  href="{{$notification->data['link']}}" class="btn badge badge-info" >View
						<i class="fas fa-search p-1"></i>
					</a>
				</span>
				<span id="deleteBtn" class="button btn col-md-2">
					<a href="{{route('notifications.delete',$notification->id)}}" class="btn badge badge-info">Mark as read
						<i class="fas fa-check p-1"></i>
					</a>
				</span>
			</div>
		</li>
		@endforeach
	</div>


@stop