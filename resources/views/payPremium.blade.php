@extends('layouts.app')
@section('content')
<style type="text/css">

.db-padding-btm {
    padding-bottom: 50px;
}
.db-button-color-square {
    color: #fff;
    background-color: rgba(0, 0, 0, 0.50);
    border: none;
    border-radius: 0px;
}
.db-button-color-square:hover {
    color: #fff;
    border: none;
}
.db-pricing-eleven {
    margin-bottom: 30px;
    margin-top: 50px;
    text-align: center;
    box-shadow: 0 0 5px rgba(0, 0, 0, .5);
    color: #fff;
    line-height: 30px;
}
.db-pricing-eleven ul {
    list-style: none;
    margin: 0;
    text-align: center;
    padding-left: 0px;
}
.db-pricing-eleven ul li {
    padding-top: 10px;
    padding-bottom: 10px;
    cursor: pointer;
}
.db-pricing-eleven ul li i {
    margin-right: 5px;
}
.db-pricing-eleven .price {
    background-color: rgba(0, 0, 0, 0.5);
    padding: 40px 20px 20px 20px;
    font-size: 60px;
    font-weight: 900;
    color: #FFFFFF;
}
.db-pricing-eleven .price small {
    color: #B8B8B8;
    display: block;
    font-size: 12px;
    margin-top: 22px;
}
.db-pricing-eleven .type {
    background-color: #52E89E;
    padding: 40px 10px;
    font-weight: 900;
    text-transform: uppercase;
    font-size: 30px;
}
.db-pricing-eleven .pricing-footer {
    padding: 10px;
}
.db-pricing-eleven.popular {
    margin-top: 10px;
}
.db-pricing-eleven.popular .price {
	padding-top: 60px;
}
      .bg-blue{
        background: #002147 !important;
      }
      .bg-pink{
        background: rgba(218,182,200,0.5)!important;
      }
      .strikethrough {
          position: relative;
        }
        .strikethrough:before {
          position: absolute;
          content: "";
          left: 0;
          top: 50%;
          right: 0;
          border-top: 3px solid;
          border-color: red;

          -webkit-transform:rotate(-60deg);
          -moz-transform:rotate(-60deg);
          -ms-transform:rotate(-60deg);
          -o-transform:rotate(-60deg);
          transform:rotate(-60deg);
        }
</style>
<link href="https://fonts.googleapis.com/css?family=Arbutus+Slab|Roboto+Slab" rel="stylesheet">

<div class="container h-100 ">
   <div class="row text-center h-25 d-flex">
            <h2 class="  text-center m-auto w-100" style="font-family: 'Roboto Slab', cursive;">Your trial period has expired!</h2>
            <h2 class="  text-center m-auto w-100" style="font-family: 'Roboto Slab', cursive;">Our pricing is Simple!</h2>
    </div>
        @if($errors->any())
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{$errors->first()}}</strong>
            </div>
        @endif


 
        <div class="row db-padding-btm db-attached h-75 d-flex">
            <div class="col-xs-12 offset-sm-2 col-sm-4 my-auto">
                <div class="db-wrapper">
                	{!! Form::open(array('route' => 'getCheckout')) !!}
                		{!! Form::hidden('type','small') !!}
                		{!! Form::hidden('pay',30) !!}
	                    <div class="db-pricing-eleven bg-pink">
                            <div class="type bg-blue">
                                6 Months
                            </div>
                            <div class="price bg-pink">
                                <span class="strikethrough float-left" style="font-size:40px">
                                    <sup>$</sup>50 
                                </span>
                            
                            </div>
	                        <div class="price bg-pink ">

	                            <sup>$</sup>30
	                        </div>

	                        <div class="pricing-footer bg-pink">
	                            <button class="btn btn-info btn-block btn-lg">Pay Now</button>
	                        </div>
	                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 my-auto">
                <div class="db-wrapper">
                	{!! Form::open(array('route' => 'getCheckout')) !!}
                		{!! Form::hidden('type','advance') !!}
                		{!! Form::hidden('pay',50) !!}
	                <div class="db-pricing-eleven bg-pink">

                        <div class="type bg-blue">
                            12 Months
                        </div>
                        <div class="price bg-pink">
                                <span class="strikethrough float-left" style="font-size:40px">
                                    <sup>$</sup>80 
                                </span>
                            
                            </div>

                        <div class="price bg-pink">
	                        <sup>$</sup>50
	                                <small></small>
	                    </div>
<!-- 	                    <ul>
	                        <li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
	                        <li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
	                        <li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
	                    </ul> -->
	                    <div class="pricing-footer bg-pink">
	                        <button class="btn btn-info btn-block btn-lg">Pay Now</button>
	                    </div>
	                </div>
	                {!! Form::close() !!}
                </div>
            </div>
        </div>
</div>
@endsection