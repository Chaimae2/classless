@extends('layouts.app')

@section('content')

    @include('messenger.partials.flash')
    <div class="list-group m-4" id=" messagesWrapper">
		<li class="list-group-item border-0 mb-1 text-info">
			<div class="row text-center align-items-center " >
				<div class="col-md-3 d-none d-md-block" >
                    <span><strong>Participant</strong></span>
                </div>		
				<span id="startTime" class="col-md-6 d-none d-md-block">
					<span ><strong>Subject</strong></span>
				</span>

				<span class="col-md-2 d-none d-md-block"><strong>date</strong></span>
			</span>
			</div>
		</li>

		
			@each('messenger.partials.thread', $threads, 'thread', 'messenger.partials.no-threads')
		

	</div>

   
@stop
