@extends('layouts.app')

@section('content')

<style type="text/css">
.card.chat-room .friend-list li {
  border-bottom: 1px solid #e0e0e0; }
  .card.chat-room .friend-list li:last-of-type {
    border-bottom: none; }

.card.chat-room img.rounded-circle {
  border-radius: 50%; }

.card.chat-room img.avatar {
  height: 3rem;
  width: 3rem; }

.card.chat-room .text-small {
  font-size: 0.95rem; }

.card.chat-room .text-smaller {
  font-size: 0.75rem; }
  </style>

    <nav aria-label="breadcrumb w-100 m-0 p-0">
      <ol class="breadcrumb  bg-light pt-4">
        <li class="breadcrumb-item">Messages</li>
        <li class="breadcrumb-item active" aria-current="page">{{$thread->subject}}</li>
      </ol>
    </nav>
<div class="card lighten-4 chat-room col-sm-12 offset-md-2 col-md-8 d-flex flex-column">
  
    <div class="card-body">

        <!-- Grid row -->
        <div class="row px-2 align-self-end">

            <!-- Grid column -->
            <div class="col-md-12 col-sm-12 col-xl-12 pr-md-4 px-lg-auto px-0">

                <div class="chat-message">

                    <ul class="list-unstyled chat">
                    	@each('messenger.partials.messages', $thread->messages, 'message')
                      @include('messenger.partials.form-message')
                    </ul>

                </div>

            </div>
            <!-- Grid column -->

           

        </div>
        <!-- Grid row -->

    </div>
</div>
@stop
