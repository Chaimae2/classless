@extends('layouts.app')

@section('content')
    <nav aria-label="breadcrumb w-100 m-0 p-0">
      <ol class="breadcrumb pt-4 bg-light">
        <li class="breadcrumb-item">New Message</li>
        <li class="breadcrumb-item active" aria-current="page">{{'To: '.$receiver->name}}</li>
      </ol>
    </nav>
<div class="card col-sm-12 offset-md-2 col-md-8 d-flex flex-column border border-0">   
    <div class="card-body col-12">
        <div class="row w-100">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block w-100">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger w-100">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>

        <!-- Grid row -->
        <div class="col-12 px-2 align-self-end w-100">
    <form action="{{ route('messages.store',$receiver->id)}}" method="post">
        {{ csrf_field() }}
        <div class="col-12">
            <!-- Subject Form Input -->
            <div class="form-group col-12">
                <label for="subject" class="col-form-label">Subject</label>
                <input type="text" class="form-control" name="subject" placeholder="Subject"
                       value="{{ old('subject') }}">
                @if ($errors->has('subject'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('subject') }}</strong>
                    </span>
                @endif
            </div>

            <!-- Message Form Input -->
            <div class="form-group col-12">
                <label for="message" class="col-form-label">Message</label>
                <textarea name="message" class="form-control" rows="8">{{ old('message') }}</textarea>

                @if ($errors->has('message'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('message') }}</strong>
                    </span>
                @endif
            </div>

<!--             <label title="{{ $receiver->name }}"><input type="checkbox" name="recipients[]"
                    value="{{ $receiver->id }}">{!!$receiver->name!!}</label>
     -->
            <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn btn-info btn-block form-control">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
@stop
