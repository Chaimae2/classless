<?php $style= $thread->isUnread(Auth::id()) ? 'background:#F0F0F0;' : ''; ?>


                    
                        
    @if($thread->participants[0]->user->id != Auth::id())
        @php($sender=$thread->participants[0]->user)
    @else
        @php($sender=$thread->participants[1]->user)
    @endif




    <style type="text/css">
        .link{
            text-decoration: none !important;
            color: inherit !important;
        }
    </style>

<li class="border-0 mb-1 list-group-item list-group-item-action" style="{{$style}}">
    <a href="{{ route('messages.show', $thread->id) }}" class="row text-center align-items-center link" >
                <div class="avatar col-md-3 " >

                    <img class="card-img-top m0" style="height: 80px;width:80px;" src="/storage/avatars/{{ $sender->avatar }}">
                    <span id="from" class="ml-4">
                        {{$sender->name}}
                    </span>
                </div> 

                <span id="Subject" class="col-md-6">
                    <strong>
                        {{ $thread->subject }}
                        @if($thread->userUnreadMessagesCount(Auth::id()) >0)
                             <b> ({{ $thread->userUnreadMessagesCount(Auth::id()) }})</b>
                        @endif
                    </strong>
                    <small class="text-muted ml-1">
                        {{ $thread->latestMessage->body }}
                    </small>
                </span>

<!--                 <span id="delete" class="col-md-1">
                    <i class="fa fa-trash "></i>
                </span> -->

                <span id="date" class="button btn col-md-2">
                    {{ $thread->latestMessage->created_at->diffForHumans() }}
             
                </span>

    </a>
    </li>

