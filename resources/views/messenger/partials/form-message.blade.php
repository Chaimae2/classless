<li class="d-flex justify-content-between mb-4">
    <form action="{{ route('messages.update', $thread->id) }}" method="post" class="chat-body white p-0 z-depth-1 w-100">
        {{ method_field('put') }}
        {{ csrf_field() }}
            <!-- Message Form Input -->
        <div class="form-group basic-textarea">
            <textarea name="message" class="form-control pl-2 my-0" id="exampleFormControlTextarea2" rows="3" placeholder="Type your message here...">{{ old('message') }}</textarea>
        </div>
        
        <div class="form-group">
            <button type="submit" class="waves-effect waves-light float-right btn btn-info btn-block form-control">Send</button>
        </div>
    </form>
</li>