
@if($message->user->id==Auth::user()->id)
<li class="d-flex justify-content-between mb-4 avatar" >
    <img src="/storage/avatars/{{ $message->user->avatar}}" alt="avatar" style="height: 80px;width:80px;" class="mr-2 ml-0 z-depth-1">
    <div class="chat-body white p-3 ml-2 z-depth-1 w-100" style="background: rgba(218,182,200,0.5)" >
        <div class="header">
            <strong class="primary-font">{{ $message->user->name }}</strong>
            <small class="pull-right text-muted">
                <i class="fa fa-clock-o"></i> 
                {{ $message->created_at->diffForHumans() }}
            </small>
        </div>
        <hr class="w-100 m-1">
        <p class="mb-0">
            {{ $message->body }}       
        </p>
    </div>
</li>

@else
<li class="d-flex justify-content-between mb-4 avatar">
    <div class="chat-body white p-3 z-depth-1 w-100" style="background: rgba(218,182,200,0.5);">
        <div class="header">
            <strong class="primary-font">{{ $message->user->name }}</strong>
            <small class="pull-right text-muted"><i class="fa fa-clock-o"></i> 
                {{ $message->created_at->diffForHumans() }}
            </small>
        </div>
        <hr class="w-100 m-1">
        <p class="mb-0">
            {{ $message->body }}
        </p>
    </div>
    <img src="/storage/avatars/{{ $message->user->avatar}} " alt="avatar" class="mr-0 ml-3 z-depth-1" style="height: 80px;width:80px;" >
</li>

@endif

