@extends('layouts.app')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Arbutus+Slab|Roboto+Slab" rel="stylesheet">
<div class="container h-100 m-0 w-100 col-md-12 p-0">
    <div class="row h-100 w-100 justify-content-center d-flex mx-auto">
      <div class="col-md-6 col-lg-8 h-100 w-100 m-0 p-0 align-self-center text-center d-none d-md-block" style="background-image:   linear-gradient(rgba(218,182,200,0.6), rgba(218,182,200,0.6)), url('/storage/images/register.jpeg');     
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center; " >
            <div class="d-flex h-100 w-100 ">
                <span class="m-auto">
                    <h3 class="m-auto" style="font-family: 'Roboto Slab', cursive; color:#002147;" >"We don't need an accurate document. We need a shared understanding."</h3>
                    <h5 class="p-2" style="font-family: 'Roboto Slab', cursive; color:#002147;"> -Jeff Patton</h5>
                </span>
            </div>
        </div>
        
        <div class="col-md-6 col-lg-4 align-self-center">
                <div class="text-center">
                    <p class="h4 mb-4">{{ __('Login') }}</p> 
                </div>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-8 offset-md-2">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="email" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-8 offset-md-2">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label pl-4" for="remember">
                                        <small>{{ __('  Remember Me') }}</small>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <!-- <div class="col-md-8 offset-md-4"> -->
                                <!-- <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button> -->

                            <div class="col-md-8 offset-md-2">
                                <a href="{{ route('password.request') }}" class="float-right">
                                        <small>{{ __('Forgot Password?') }}</small>
                                </a>
                                <button type="submit" class="btn btn-info btn-block my-4">
                                    {{ __('Login') }}
                                </button>

                                <p class="text-center">
                                    Don't have an account yet?
                                    <a href="{{ route('register') }}">
                                        <strong>{{ __('Register Now!') }}</strong>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </form>
        </div>

    </div>
</div>
@endsection
