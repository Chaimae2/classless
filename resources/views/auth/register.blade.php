@extends('layouts.app')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Arbutus+Slab|Roboto+Slab" rel="stylesheet">

<div class="container h-100 m-0 w-100 col-md-12 p-0" >
    <div class="row h-100 w-100 justify-content-center d-flex mx-auto">

        <div class="col-md-6 col-lg-8 h-100 w-100 m-0 p-0 align-self-center text-center d-none d-md-block" style="background-image:   linear-gradient(rgba(218,182,200,0.6), rgba(218,182,200,0.6)), url('/storage/images/register.jpeg');     
            background-size: cover;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: center; " >
            <div class="d-flex h-100 w-100 ">
                <span class="m-auto ">
                    <h3 class="m-auto" style="font-family: 'Roboto Slab', cursive; color:#002147;" >"We don't need an accurate document. We need a shared understanding."</h3>
                    <h5 class="p-2" style="font-family: 'Roboto Slab', cursive; color:#002147;"> -Jeff Patton</h5>
                </span>
            </div>
        </div>

        <div class="col-md-6 col-lg-4 align-self-center h-100 w-100 m-0 p-0 d-flex">
                    <form method="POST" action="{{ route('register') }}" class="text-center border border-light p-5 w-100 border border-0 my-auto">
                        @csrf
                        <p class="h4 mb-4">{{ __('Register') }}</p>
                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Nickname" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email Address" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback row">

                            <div class="col-md-12 input-group">
                                <input id="password" type="password" class="border border-right-0 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                                <span class="input-group-btn" style="background-color: white;">
                                    <button class="btn  px-2 py-1 text-center border border-0" type="button" style="background-color: white;">
                                       <i toggle="#password" class="fa fa-fw fa-eye toggle-password" style="pointer-events: initial;">  </i>
                                   </button>
                                </span>


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    
                            </div>

                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="school" type="text" class="form-control{{ $errors->has('school') ? ' is-invalid' : '' }}" name="school" value="{{ old('school') }}" placeholder="School Name" required autofocus>

                                @if ($errors->has('school'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('school') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="country" type="text" class="form-control{{ $errors->has('Country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}" placeholder="Country" required autofocus>

                                @if ($errors->has('country'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row ">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-info btn-block my-4">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                        <!-- Register -->
                        <p>
                            <small>Already a member?</small>
                            <a href="{{route('login')}}">
                                <STRONG>Login Now!</STRONG>
                            </a>
                        </p>
                    </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
@endsection
