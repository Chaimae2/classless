@extends('layouts.app')

@section('content')


<div class="wrapper m-3">
    @if(session()->has('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session()->get('success') }}</strong>
        </div>
    @endif

    <div class="row text-center mx-auto card text-center" data-intro='Find teachers matching your criteria.'>
    <form action="/home", method="GET" class="row card-body p-0 pt-3 text-center" >
        <div class="col-md-2 col-sm-6 offset-md-1 form-group">
            <input name="country" placeholder="Country" class="form-control">  
        </div>

        <div class="col-md-2 col-sm-6 form-group">
            <input name="language" placeholder="Native language" class="form-control">
        </div>
        
        <div class="col-md-2 col-sm-6 form-group">
            <input name="min-age" placeholder="Min Students' age" class="form-control"> 
        </div>

        <div class="col-md-2 col-sm-6 form-group">
            <input name="max-age" placeholder="Max Students' age" class="form-control"> 
        </div>
        <div class="col-md-3 form-group">
            <button type="submit" class="btn">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </form>
    </div>
    <div class="row justify-content-left">
        @foreach($users as $key=>$user)
        @if($user->role_id!=1)
        @if($key==0)
        <div class="col-lg-4 col-md-6 d-flex text-center ">
            <div class="card m-3 w-100">
                <div class="cardheader bg-info text-light mb-0 pb-0" >
                    <a href="{{route('profile.browse',Hashids::encode($user->id))}}" class="text-dark float-right px-4 py-2" style="text-decoration:none;" data-intro='View the profile to find out more.'>
                        <i class="fa fa-search"></i>
                        View
                    </a>
                    <a href="{{route('messages.create',Hashids::encode($user->id))}}" class="text-dark float-left px-4 py-2" style="text-decoration:none;" data-intro='Send messages to discuss your collaboration idea.'>
                        <i class="fa fa-envelope"></i>
                    Contact
                    </a>
                </div>
                <div class="avatar mt-2" >
                    <img class="card-img-top ml-3 mt-0" style=" height:100px;width:100px;" src="/storage/avatars/{{ $user->avatar }}">
                </div>
                <div class="card-body">
                    <p class="card-title">
                        <i class="fas fa-user"></i>
                        {{ $user->name }}</p>
                    <p class="card-text">
                        <i class="fas fa-school"></i>
                        {{ $user->school }}
                    </p>
                    <p class="card-text ">
                        <i class="fas fa-map-marker-alt"></i>
                        {{ $user->country }}
                    </p>
                
                    @if(Auth::user()->videorequests_ongoing()->where('to', $user->id)->first() || Auth::user()->videorequests_ongoing()->where('from',$user->id)->first())
                        <a href="{{ route('room',Hashids::encode($user->id)) }}" class="btn btn-info btn-block">Join the video session</a>
                    @else
                    <a href="" class="btn btn-info btn-block invisible">Call</a>
                        
                    @endif
                    <a href="{{ route('videorequests.create',Hashids::encode($user->id)) }}" class="btn btn-info btn-block" data-intro='Request a common video session. If approved, you will be able to call each other during the period that was set for the video session. Timezones are handled in the background.'>Request video session</a>
                    
                 </div>
            </div>
        </div>
        @else
        <div class="col-lg-4  col-md-6 d-flex text-center ">
            <div class="card m-3 w-100" >
                <div class="cardheader bg-info text-light mb-0 pb-0" >
                    <a href="{{route('profile.browse',Hashids::encode($user->id))}}" class="text-dark float-right px-4 py-2" style="text-decoration:none;">
                        <i class="fa fa-search"></i>
                        View
                    </a>
                    <a href="{{route('messages.create',Hashids::encode($user->id))}}" class="text-dark float-left px-4 py-2" style="text-decoration:none;">
                        <i class="fa fa-envelope"></i>
                    Contact
                    </a>
                </div>
                <div class="avatar mt-2" >
                    <img class="card-img-top ml-3 mt-0" style=" height:100px;width:100px;" src="/storage/avatars/{{ $user->avatar }}">
                </div>
                <div class="card-body">
                    <p class="card-title">
                        <i class="fas fa-user"></i>
                        {{ $user->name }}</p>
                    <p class="card-text">
                        <i class="fas fa-school"></i>
                        {{ $user->school }}
                    </p>
                    <p class="card-text ">
                        <i class="fas fa-map-marker-alt"></i>
                        {{ $user->country }}
                    </p>
                
                    @if(Auth::user()->videorequests_ongoing()->where('to', $user->id)->first() || Auth::user()->videorequests_ongoing()->where('from',$user->id)->first())
                        <a href="{{ route('room',Hashids::encode($user->id)) }}" class="btn btn-info btn-block">Join the video session.</a>
                    @else
                    <a href="" class="btn btn-info btn-block invisible">Call</a>
                        
                    @endif
                    <a href="{{ route('videorequests.create',Hashids::encode($user->id)) }}" class="btn btn-info btn-block">Request video session</a>
                    
                 </div>
            </div>
        </div>
        @endif
    @endif
     @endforeach
</div>
</div>

    </script>
@endsection
