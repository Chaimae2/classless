<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Support\Facades\Auth;
use App\videorequest;
use Carbon\Carbon;
use DB;

class User extends \TCG\Voyager\Models\User  implements MustVerifyEmail,CanResetPassword
{
    use Notifiable;
    use Messagable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','school','country','mother_tongue','about','min_age', 'max_age','first_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function threads()
    {
        return $this->hasMany('App\Thread');
    }

    public function payments()
    {
        return $this->hasMany('App\payment','user_id');
    }

    public function languages()
    {
        return $this->belongsToMany('App\language','languages_users');
    }

    public function mother_tongue()
    {
        return language::find($this->mother_tongue);
    }

    public function language1()
    {
        return $this->languages()->where('type','=','1')->first();
    }

    public function language2()
    {
        return $this->languages()->where('type','=','2')->first();
    }



    public function payments_ongoing(){
        return $this->payments()->where('end_date', '>', Carbon::now())->get();
    }

    public function videorequests()
    {
        $id=Auth::user()->id;
        return videorequest::where('from', 'like', $id)
        ->orWhere('to', 'like', $id)->latest()->get();
    }

    public function videorequests_sent()
    {
        return $this->hasMany('App\videorequest','from');
    }

    public function videorequests_received()
    {
        return $this->hasMany('App\videorequest','to');
    }

    public function videorequests_approved()
    {
        return $this->videorequests()->where('status', '=', 1);
    }

    public function videorequests_waiting()
    {
        return $this->videorequests_received()->where('status', '=', 0);
    }

    public function videorequests_ongoing()
    {   
        $ongoing=$this->videorequests_approved()
        ->where('start_time', '<=', Carbon::now())
        ->where('end_time', '>=', Carbon::now());
        return $ongoing;
    }

    public function scopeFilter($q)
    {
        if (request('country')) {
            $q->where('country', 'like', request('country'));
        }
        if (request('min-age')) {
            $q->where('min_age', '>=', request('min-age'));
        }
        if (request('max-age')) {
            $q->where('max_age', '<=', request('max-age'));
        }
        if (request('language')) {
            $q->where('mother_tongue', 'like', request('language'));
        }

        $q->where('id', '!=', Auth::id());
        return $q;
    }

}
