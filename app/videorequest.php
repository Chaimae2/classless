<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class videorequest extends Model
{
    //
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
        //status: 0=waiting, 1=Approved
    protected $fillable = [
        'from', 'to','start_time','end_time','status','details'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function receiver()
    {
        return $this->hasOne('App\User','id','to');
    }

    public function sender()
    {
        return $this->hasOne('App\User','id','from');
    }

    public function getEndDate()
    {
        return Carbon::parse($this->end_time);
    }

    public function getStartDate()
    {
        return Carbon::parse($this->start_time);
    }


}
