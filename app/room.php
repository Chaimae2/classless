<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class room extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'participant1', 'participant2', 'name',
    ];
}
