<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ParticipantJoined extends Notification implements ShouldQueue
{
    use Queueable;
    protected $participant;
    protected $roomLink;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($partcipant,$roomlink)
    {
        //
        $this->participant=$participant;
        $this->roomLink=$roomLink;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->participant->name. ' is waiting you in your common virtual classroom.',
            'link' => url($this->link)
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message' => $this->participant->name. ' is waiting you in your common virtual classroom. Do not be late.',
            'link' => url($this->link)
        ]);
    }
}
