<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class MessageRcd extends Notification
{
    use Queueable;
    protected $sender;
    protected $threadId;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($sender,$threadId)
    {
        //
        $this->sender=$sender;
        $this->threadId=$threadId;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => $this->sender->name. ' sent you a new message.',
            // 'link' => '\'messages.show\','.$this->threadId
            'link' => "messages"
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'message' => $this->sender->name. ' sent you a new message.',
            // 'link' => '\'messages.show\','.$this->threadId
            'link' => "messages"
        ]);
    }
}
