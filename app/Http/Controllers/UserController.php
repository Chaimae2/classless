<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\language;
use DB;
use Storage;
use Vinkla\Hashids\Facades\Hashids;

class UserController extends Controller
{

    public function browse($id)
    {
    	$id=Hashids::decode($id)[0];
        $user = User::where('id', '=', $id)->first();
        return view('profile.browse',compact('user',$user));
    }

    public function edit(){
    	$user = Auth::user();
    	$languages = language::get();
    	return view('profile.edit',compact('user','languages'));
    }

    public function update(Request $request){
    	$user = Auth::user();

		$request->validate([
	        'password' => 'string|min:6|dumbpwd',
	        'school'=> 'nullable|string|max:255',
	        'country'=> 'nullable|string|max:255',
	        'about'=> 'nullable|string|max:255',
	        'min-age'=>'nullable|integer',
	        'max-age'=>'nullable|integer'
		]);
 
		if ($request->hasFile('avatar')) {
		   	$request->validate([
				'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			]);
			$avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();
			$request->avatar->storeAs('avatars',$avatarName);
			if($user->avatar!='user.jpg')
				$this->delete_avatar($user->avatar);
			$user->avatar = $avatarName;
		}

		if( $request->input('email') != $user->email){
			$request->validate([
			'email' => 'string|email|max:255|unique:users',
			]);
			$user->email =  strtolower($request-> input('email'));
		}

		if( $request->input('name') != $user->name){
			$request->validate([
				'name' => 'string|max:255|unique:users',
			]);
			$user->name =  $request->input('name');
		}

		if (  $request->input('password') != '*******')
	    {
	        $user->password = Hash::make($request->input('password'));
	    }
		
		$user->school =  $request->input('school');
    	$user->country =  $request->input('country');
   		$user->about =  $request->input('about');
   		$user->mother_tongue = $request->input('mother-tongue');
   		$user->min_age =  $request->input('min-age');
   		$user->max_age =  $request->input('max-age');
   		$user->save();


		//Other spoken languages
		$id1=$request->input('language1');
   		if($id1!="" && !Auth::user()->languages()->where('language_id','=',$id1)->exists())
   		{
	   		$language1 = language::find($id1);
	   		$user->languages()->attach($language1,['type'=>'1']);
   		}
   		//Delete already set language1 if no option is selected
   		else if($id1=="" && $user->language1()!=null){
   			$user->languages()->detach($user->language1()->id);
   		}
		
		$id2=$request->input('language2');
   		if($id2!="" && !Auth::user()->languages()->where('language_id','=',$id2)->exists())
   		{
	   		$language2 = language::find($id2);
	   		$user->languages()->attach($language2,['type'=>'2']);
   		}
   		//Delete already set language2 if no option is selected
   		else if($id2=="" && $user->language2()!=null){
   			$user->languages()->detach($user->language2()->id);
   		}
		
		return back()->withInput()
		->with('success','You have successfully edited your personal information.');
	}

	public function delete_avatar($avatar)
	{
	    Storage::disk('public')->delete('/avatars/'.$avatar);    
	}

	public static function setFirstLogin(){
		Auth::user()->first_login=1;
		Auth::user()->save();
	}

}
