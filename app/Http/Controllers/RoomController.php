<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\room;
use Illuminate\Support\Facades\Auth;
use Vinkla\Hashids\Facades\Hashids;

class RoomController extends Controller
{
    //

    public function index($participant2){
        $participant2=Hashids::decode($participant2)[0];
    	$participant1=Auth::id();

    	if($participant1<$participant2)
    		$roomName=$participant1.$participant2.'@classroomschat.io';
    	else 
    		$roomName=$participant2.$participant1.'@classroomschat.io';

    	$room=room::where('name', '=', $roomName)->first();

    	if ($room==null){
    		$this->store($participant1,$participant2,$roomName);
    	}

    	return view('partials.jitsiFrame', compact('roomName'));

    }

    public function store($participant1,$participant2,$roomName){

	        room::Create([
	            'participant1' => $participant1,
	            'participant2'=>$participant2,
	            'name'=>$roomName,
	        ]);
   
    }
}
