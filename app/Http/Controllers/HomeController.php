<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\videorequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //All users except the authenticated one
        // $users=User::where('id', '!=', Auth::id())->get();
        $users=User::filter()->get();
        $user = Auth::user();

        return view('home',compact('users','user','videoRequests'));
    }

}
