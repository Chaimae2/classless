<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Vinkla\Hashids\Facades\Hashids;

class NotificationsController extends Controller
{
    //
    public function index(){
    	return view('notifications');
    }

    public function markRead($id,$link=null){
        
    	Auth::user()->unreadNotifications->where('id', $id)->markAsRead();
    	if($link!=null){
    		return redirect()->route($link);
        }
    	else 
    		return redirect()->back();
    }


    public function deleteAll(){
        Auth::user()->notifications()->delete();
        return redirect()->back();
    }

    public function delete($id){
        Auth::user()->notifications->where('id', $id)->delete();
        return redirect()->back();
    }
}
