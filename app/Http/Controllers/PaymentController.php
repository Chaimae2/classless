<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\payment;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    //


    
    public function store(Request $request){
    	if($request->amount>=40){
	    	$type=1;
	    	$end=Carbon::now()->addMonths(6);                 
	    }
	    else{
	    	$type=0;
	    	$end=Carbon::now()->addYear(); 
	    }
	    // dd($end);
	    $transaction_id=$request->transaction_id;
    	$payment=payment::Create([
            'user_id'=>Auth::id(), 
            'transaction_id'=> $transaction_id,
            'type'=> $type,
            'end_date'=>$end,
        ]);
    	return redirect()->route('home')->withSuccess('Thank you for paying. We really appreciate it!');
    }
}
