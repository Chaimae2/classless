<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\videorequest;
use App\User;
use App\Notifications\videoRequestRcd;
use App\Notifications\VideorequestCanceled;
use App\Notifications\videoRequestAccepted;
use App\Notifications\VideosessionStarted;
use JamesMills\LaravelTimezone\Facades\Timezone;
use Vinkla\Hashids\Facades\Hashids;
use Carbon\Carbon;


class VideorequestController extends Controller
{
    public function index(){
        //delete past requests
        $videorequests=videorequest::where('end_time', '<', Carbon::now())->get();
        foreach($videorequests as $videorequest){
           $videorequest->delete();
        }

        $user = Auth::user();
        return view('videorequests.index',compact('user'));
    }

    public function create($to)
    {
        $to=Hashids::decode($to)[0];
        $receiver=User::where('id','=',$to)->first();
        return view('videorequests.create',compact('receiver'));
    }

    public function store(Request $request,$to)
    {
        $input = Input::all();
        $start=$input['startTime'];
        $end=$input['endTime'];
        $details=$input['details'];

        $request->validate([
            'startTime' => 'required',
            'endTime'=> 'required'
        ]);

        $id=$videorequest=videorequest::Create([
            'from'=>Auth::id(), 
            'to'=>$to,
            'start_time'=>Timezone::convertFromLocal($start),
            'end_time'=>Timezone::convertFromLocal($end),
            'details'=>$details,
        ]);

        $senderId=Auth::id();
        $receiver=User::where('id','=',$to)->first();
        if(videorequest::find($id)->first()->exists()){
            $receiver->notify(new videoRequestRcd($senderId));
        }

        return redirect()->route('home');
    }

    public function update($id)
    {
        $videorequest=videorequest::where('id', '=', $id)->first();
        //Ajax

        return redirect()->route('home');
    }

    public function delete($id)
    {
        $videorequest=videorequest::where('id', '=', $id)->first();
        $videorequest->delete();

        return redirect()->back();
    }

    //Cancel videorequest sent by auth user;
    public function cancel($id){
        $videorequest=videorequest::where('id', '=', $id)->first();
        $sender=Auth::user();

        //notify
        if(Auth::user()==$videorequest->receiver)
            $userToNotify=$videorequest->sender;
        else
            $userToNotify=$videorequest->receiver;
        $userToNotify->notify(new VideorequestCanceled($sender,$videorequest->start_time));
        //delete
        $this->delete($id);

        return redirect()->back();
    }

    //Update status when request approved
    public function approve($id)
    {
        $videorequest=videorequest::where('id', '=', $id)->first();
        $videorequest->status=1;
        $videorequest->save();
                //notify
        $userToNotify=$videorequest->sender;
        $userToNotify->notify(new videoRequestAccepted(Auth::user(),$videorequest->start_time));

        $when=$videorequest->start_time;
        
        // $userToNotify->notify((new VideosessionStarted('/'))->delay($when));
        // Auth::user()->notify((new VideosessionStarted('/'))->delay($when));
        return redirect()->back();
    }

    public function calendar(){
        //delete past requests
        $videorequests=videorequest::where('end_time', '<', Carbon::now())->get();
        foreach($videorequests as $videorequest){
           $videorequest->delete();
        }
        return view('calendar');
    }


}
