<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

//Checks if paying or trial period
class premium
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $diff = Auth::user()->created_at->diffInDays(Carbon::now());
        if (Auth::user()->payments_ongoing()->isEmpty() && $diff>30 ) {
            return redirect()->route('payPremium');
        }
        return $next($request);
    }
}
