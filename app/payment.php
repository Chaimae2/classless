<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class payment extends Model
{
    //

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //type: 0:6months , 1:1year
    protected $fillable = [
        'user_id','transaction_id','type','end_date'
    ];
}
