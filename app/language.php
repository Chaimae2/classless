<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class language extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
        //status: 0=waiting, 1=Approved
    protected $fillable = [
        'name'
    ];

    // public function users()
    // {
    //     return $this->belongsToMany('App\user','id');
    // }
}
